// Released under MIT License
// See LICENSE file included
// Copyright 2023 Tim Watts <tw_gitlab@dionic.net>

// PinDebounce debouncing class using vertical counters: https://www.compuphase.com/electronics/debouncing.htm

#include <Arduino.h>

class PinDebounce
{
private:
  uint32_t pinMask;
  uint32_t c0;
  uint32_t c1;
  uint32_t c2;
  uint32_t debouncedPinState;
  uint32_t lastDebouncedPinState;

public:

  // Constructor
  PinDebounce( uint32_t pinMask );

  void onTimer();

  // return all pin states subject to pinMask so that non watched pins are stable 0
  uint32_t readPins();

  // return state (1 = pressed, 0 not pressed) for a single pin, subject to pinMask
  // pin outside of pinMask will always return 0
  uint8_t readPin(uint8_t pinNum);

  // demo function - not needed for production code - maybe...
  uint32_t pinsChanged();

  bool pinChanged(uint8_t pin);

};
