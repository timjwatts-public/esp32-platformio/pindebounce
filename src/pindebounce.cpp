// Released under MIT License
// See LICENSE file included
// Copyright 2023 Tim Watts <tw_gitlab@dionic.net>

// PinDebounce debouncing class using vertical counters: 
// Based on a blog post explaining it here: https://www.compuphase.com/electronics/debouncing.htm
// This currently supports upto 3 vertical bits of counter, so requires 8 iterations of the timer
// for a pin state to be regarded as stable

#include "pindebounce.h"

// Constructor
PinDebounce::PinDebounce( uint32_t PinMask )
  {
    pinMask = PinMask;
    c2 = 0x00; // 32 sets of bit 2 of vertical counter MSB[GPIO31 GPIO30 ... GPIO2 GPIO1 GPIO0]LSB
    c1 = 0x00; // 32 sets of bit 1 of vertical counter MSB[GPIO31 GPIO30 ... GPIO2 GPIO1 GPIO0]LSB
    c0 = 0x00; // 32 sets of bit 0 of vertical counter MSB[GPIO31 GPIO30 ... GPIO2 GPIO1 GPIO0]LSB
    debouncedPinState = 0x00;
    lastDebouncedPinState = 0x00;
  }

void PinDebounce::onTimer()
{
  uint32_t samples;
  uint32_t pinChanged;

  // Read in all 32 GPIO bits as inputs whether enabled of not - single register read operation,
  // more efficient than a loop. Other inputs outside of pinMask may do random stuff
  // but that's harmless
  samples = REG_READ(GPIO_IN_REG);

  // 1 if current pin is different to the last debounced state
  // bitwise logic, so doing all 32 GPIOs at once, no loops
  pinChanged = samples ^ debouncedPinState;

  // This manages the vertical counters for all 32 GPIOs, to a depth of 3 bits = 8 counter iterations
  // that are required for an input to be considered stable
  //
  // bitwise add +1, 3 bits so count of 8
  c2 = c2 ^ (c1 & c0);
  c1 = c1 ^ c0;
  c0 = ~c0;

  // Reset counter to zero if pin is same as last debounced state. all 32 pins computed at once
  c2 &= pinChanged;
  c1 &= pinChanged;
  c0 &= pinChanged;

  // Change the debounced state if and only if pin has changed AND counter is zero, which only happens after the counter wraps around
  debouncedPinState ^= (pinChanged & ~(c0 | c1 ));
}

// return all pin states subject to pinMask so that non watched pins are stable 0
uint32_t PinDebounce::readPins()
{
  return debouncedPinState & pinMask;
}

// return state (1 = pressed, 0 not pressed) for a single pin, subject to pinMask
// pin outside of pinMask will always return 0
uint8_t PinDebounce::readPin(uint8_t pinNum)
{
  return ((debouncedPinState & pinMask) >> pinNum) & 1;
}

// demo function - not needed for production code - maybe...
uint32_t PinDebounce::pinsChanged()
{
  uint32_t changes;

  changes = (debouncedPinState ^ lastDebouncedPinState) & pinMask;
  if (changes)
  {
    lastDebouncedPinState = debouncedPinState;
  }
  return changes;
}

bool PinDebounce::pinChanged(uint8_t pin)
{
  if (pinsChanged() & (1 << pin))
  {
    return true;
  }
  return false;
}


